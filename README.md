ECOMMERCE WEBSITE 

Contains cucumber features for amazon ecommerce website.

##Overview 

Behavior Driven scenarios written in Cucumber for amazon ecommerce website.

Selenium-WebDriver is used as a backend tool for driving the browsers. This automation suite is compatible with following browsers

Google Chrome
Mozilla Firefox
Internet Explorer
Opera

This suite runs scenario as:

As a logged in member I want to search for top 5 brands of any category and add to shopping cart proceed until payment page

##Purpose 

Major purpose of this example automation is to understand how one can make maximum usage of cross browser testing with minimum effort by following some best practices mentioned here. Few Best Practices are as follows:

How to initialize different browsers
How to write appropriate setup and teardown's
How to take failed scenario screenshots
How to create json and html reports which helps TestNow to create consolidated reports for you.
How to organize code in case of Java Cucumber setup
and few more which you can relate to ... :)

##How To Use This For Your Application 
A walk through the folder structure

src/test/java/features folder is the place where all the test case descriptions are kept in Given-When-Then(GWT) format. Cucumber features
src/test/java/com/java/cukes contains all the step definitions where 1-1 mapping is present between cucumber scenarios and its definitions
Relation between Scenario and Step_Definitions folder is maintained in CukesRunnerTest.java file along with the reporting formats as CucumberOptions
CukesRunnerTest.java file is the entry point to test execution
Hooks.java contains the setup and teardown methods
Reports folder is created inside of target folder
Utilities.java contain common utility methods frequently used by the steps

Do's

Change the feature files in src/test/features folder as per the test cases in your project
Make appropriate step_definitions for the new scenarios added by you in the src/test/java/com/java/cukes folder
Add more utility methods as required by your projects in Utilities.java file

Dont's

Do not delete pom.xml file as it acts as the main config file for java-maven based project
Do not delete or be careful while modifying CukeRunnerTest.java, doing so might stop execution and report generation
Do not delete anything from Hooks.java as it contains the setup and teardown which has browser initializations processes and screenshots taking methodology. You can add more intermediate methods like BeforeAll, AfterAll etc.

##Requirements 

Code
Java 8

##Programming Language

Java 8

##Dependencies

All mentioned in pom.xml file, no additional dependency installation required
Browsers
Google Chrome
Mozilla Firefox
Internet Explorer
Opera
WebDrivers

chromedriver -- put in any folder included in PATH variable (777 to avoid permission issues)

operadriver -- mandatorily put in /usr/local/bin/operadriver (777 to avoid permission issues)

iedriver -- put in any folder inlcuded in PATH variable (777 to avoid permission issues)

##Setup

git clone https://gitlab.com/NikhitaD/ecommercewebsiteautomation.git


##Execution

Commmands
export BROWSER=FireFox -Its just an example but you can use any browser
export TEST_URL='https://www.amazon.in/'-This will run your automation again this page URL

mvn clean -- This will clean the old compiled code
mvn test -- This will recompile the code and execute the tests
mvn clean test -- This will do 1 and 2 both

##Reporting

Reports are created in following 2 types of format

HTML : index.html
JSON : index.json
Reports are created inside the target/reports directory with above mentioned filenames

