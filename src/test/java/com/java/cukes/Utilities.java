/* 
 * File Name: Utilities.java
 * Copyright 2015, Opex Software
 * Apache License, Version 2.0
 * This file contains the utility methods frequently used by the step definitions
 */


package com.java.cukes;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import com.java.cukes.Hooks;


public class Utilities {

	//This method created to to click on element
	public static void clickOnElement(final By propertyvalue) {
		Utilities.waitForElementPresence(propertyvalue);
		Hooks.driver.findElement(propertyvalue).click();
		sleepTime();
	}

	//This method created to to enter text
	public static void enterText(final By propertyvalue,String text) {
		Hooks.driver.findElement(propertyvalue).clear();
		Hooks.driver.findElement(propertyvalue).sendKeys(text);
		sleepTime();
	}


	//This method created to wait for element presence.
	public static WebElement waitForElementPresence(final By propertyValue){
		WebDriverWait wait = new WebDriverWait(Hooks.driver, 600);
		return wait.until(ExpectedConditions.presenceOfElementLocated(propertyValue));

	}


	//This method created to get text.
	public static String getElementText(final By propertyValue){
		Utilities.waitForElementPresence(propertyValue);
		return Hooks.driver.findElement(propertyValue).getText();
	}

	//This method counting row.
	public static int countRow(final By propertyValue){
		Utilities.waitForElementPresence(propertyValue);
		//System.out.println("inside countRow function");
		List<WebElement> elements = Hooks.driver.findElements(propertyValue);
		//System.out.println(elements);
		int rowCount = elements.size();
		System.out.println("Number of Elements Displayed "+rowCount);
		return rowCount;
	}

	//This method created for check element is present.
	public static Boolean isElementPresent(final By propertyValue){
		Hooks.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		Boolean elementPresent = true;
		if (Hooks.driver.findElements(propertyValue).isEmpty()){
			elementPresent = false;
			System.out.println("Element Not Present.");
		};
		Hooks.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return elementPresent;

	}

	//This method created to wait for random second.
	public static void waitRandomMs(){
		Random random = new Random();
		int timeRange = 5000 - 2000 + 1;
		int waitTime = random.nextInt(timeRange) + 1000;
		try {
			Thread.sleep(waitTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	//This method will perform the action of a mouse hover over the specified element.
	public static void mouseOver(final By propertyValue){
		Utilities.waitForElementPresence(propertyValue);
		WebElement hoverOverElement;
		Actions builder = new Actions(Hooks.driver);
		hoverOverElement = Hooks.driver.findElement(propertyValue);	    
		builder.moveToElement(hoverOverElement).build().perform();
	}


	//This method created to scroll to element
	public static WebElement scrollIntoViewElement(final By propertyValue){
		WebElement element = Hooks.driver.findElement(propertyValue);
		((JavascriptExecutor) Hooks.driver).executeScript(
				"arguments[0].scrollIntoView();", element);
		return null;
	}


	//This method created to switch to new windows
	public static void switchToNewWindowAndValidate(){
		// Store the current window handle
		String winHandleBefore = Hooks.driver.getWindowHandle();
		// Switch to new window opened
		for(String winHandle : Hooks.driver.getWindowHandles()){
			//		      	      driver.switchTo().window(winHandle);
			Hooks.driver.switchTo().window(winHandle);
		}
		sleepTime();
		Utilities.clickOnElement(By.id("add-to-cart-button"));
		sleepTime();
		// Close the new window, if that window no more required
		Hooks.driver.close();
		// Switch back to original browser
		Hooks.driver.switchTo().window(winHandleBefore);
	}


	static void sleepTime(){
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

