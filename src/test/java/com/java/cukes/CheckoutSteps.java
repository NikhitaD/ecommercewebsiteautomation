
package com.java.cukes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import com.github.javafaker.Faker;
import com.java.cukes.Utilities;


public class CheckoutSteps {	
	public static WebDriver driver;
	private String landingPageURL;
	public static By logo = By.xpath("//a[@class='nav-logo-link']");
	public static By searchBox = By.id("twotabsearchtextbox");
	public static By searchIcon = By.xpath("//input[@value='Go']");
	public static By loginEmailTextBox = By.id("ap_email");
	public static By loginPasswordTextBox = By.id("ap_password");
	public static By pincodeTextBox = By.id("enterAddressPostalCode");
	public static By addressLine1TextBox = By.id("enterAddressAddressLine1");
	public static By addressLine2TextBox = By.id("enterAddressAddressLine2");
	public static By landmarkTextBox = By.id("enterAddressLandmark");
	public static By rowXpath = By.xpath("//img[@class='s-image']");
	public static By signInButton = By.xpath("//span[contains(@class,'nav-line-1') and text()='Hello. Sign in']");
	public static By cartIcon = By.xpath("//span[text()='Cart']");


	public CheckoutSteps()
	{
		landingPageURL = System.getenv("LANDINGPAGE_URL");
		if(landingPageURL == null){
			landingPageURL = System.getenv("TEST_URL");
		}
		driver = Hooks.driver;
	}

	@Then("^I am amazon homepage$")
	//Validate amazon homepage
	public static void validateHomepage() throws IOException {
		Boolean homepageTextPresence = Utilities.isElementPresent(logo);
		assertTrue(homepageTextPresence);
		WebElement logoNameXpath= driver.findElement(logo);
		String logoName = logoNameXpath.getAttribute("aria-label");
		assertEquals(logoName, "Amazon");		
	}

	@Given("^I am on landing page$")
	//Open landing page 
	public void I_am_on_testdrive_login_page() throws Throwable {		
		driver.get(landingPageURL);
	}

	@And("^I search for Mobile$")
	//Search for mobile category
	public static void searchTextInSearchBox() {
		Utilities.enterText(searchBox, "top 5 mobiles");
		Utilities.clickOnElement(searchIcon);
		sleepTime();
	}

	@And("^I select top 5 brands$")
	public static void selectBrands() {
		int searchResultsCount = Utilities.countRow(rowXpath);
		System.out.println(searchResultsCount);
		for(int i=1;i<=6;i++) {
			Utilities.scrollIntoViewElement(By.xpath("(//img[@class='s-image'])["+i+"]"));
			sleepTime();
			Utilities.clickOnElement(By.xpath("(//img[@class='s-image'])["+i+"]"));
			sleepTime();
			Utilities.switchToNewWindowAndValidate();
			sleepTime();
		}
	}

	@And("^I click on sign in button$")
	public static void clickOnSignIn() {
		Utilities.clickOnElement(signInButton);
		sleepTime();
	}

	@And("^I enter details$")
	public static void enterDetails() throws IOException {
		FileInputStream openFile = new FileInputStream(new File("src/test/resources/UserData.xlsx")); 
		XSSFWorkbook srcBook = new XSSFWorkbook(openFile);     
		XSSFSheet sourceSheet = srcBook.getSheet("Sheet1");
		int rownum=1;
		XSSFRow sourceRow = sourceSheet.getRow(rownum);
		XSSFCell cell1=sourceRow.getCell(0);
		String newCell1 = cell1.getRawValue();
		Utilities.enterText(loginEmailTextBox, newCell1);
		Utilities.clickOnElement(By.id("continue"));
		XSSFCell cell2=sourceRow.getCell(1);
		String newCell2 = cell2.getStringCellValue();
		Utilities.enterText(loginPasswordTextBox, newCell2);
		Utilities.clickOnElement(By.id("signInSubmit"));
		sleepTime();
	}

	@And("^I proceed with checkout$")
	public static void proceedCheckout() throws IOException {
		Utilities.clickOnElement(cartIcon);
		sleepTime();
		Utilities.clickOnElement(By.name("proceedToRetailCheckout"));
		CheckoutSteps.enterDetails();
	}

	@Then("^I validate \"([^\\\"]*)\" page$")
	//Validate address page
	public static void validateAddressPage(String pageName) {
		String addressPageHeader = Utilities.getElementText(By.xpath("//h1[@class='a-spacing-base']"));
		if(pageName.contains("address"))
			assertEquals(addressPageHeader, "Select a delivery address");
		else if(pageName.contains("shipment")) {
			assertEquals(addressPageHeader, "Choose your delivery options");

		}
		else {
			assertEquals(addressPageHeader, "Select a payment method");
		}
	}

	@And("^I fill address details$")
	//Fill address details to address page
	public static void enterAddressDetails() {
		Faker faker = new Faker();
		Utilities.enterText(pincodeTextBox, "411014");
		sleepTime();
		Utilities.enterText(addressLine1TextBox, faker.address().streetAddress());
		Utilities.enterText(addressLine2TextBox, faker.address().streetName());
		Utilities.enterText(landmarkTextBox, faker.address().citySuffix());
		CheckoutSteps.clickContinueButton();
		sleepTime();
		CheckoutSteps.clickContinueButton();
		sleepTime();
	}

	@And("^I click on continue button$")
	//Click on continue button
	public static void clickContinueButton() {
		Utilities.clickOnElement(By.xpath("(//input[@value='Continue'])[1]"));
		sleepTime();
	}

	static void sleepTime(){
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}